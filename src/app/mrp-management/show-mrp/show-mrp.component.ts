import { XmlParser } from '@angular/compiler';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-mrp',
  templateUrl: './show-mrp.component.html',
  styleUrls: ['./show-mrp.component.css']
})
export class ShowMRPComponent implements OnInit {
 
  @ViewChild("agGrid", {static: false }) agGrid: AgGridAngular;
  private gridApi:any=[];
  private gridColumnApi:any=[];
  private items: any=[];

  constructor(private service:SharedService) { }
 
  rowData:any=[];

  columnDefs = [
    {headerName:"Supplier",field:"Supplier", sortable: true, rowGroup: true,},
    {headerName:"Stock",field:"StockCode", sortable: true},
    {headerName:"Stock Name",field:"StockName", filter: true, resizable: true},
    {headerName:"Last Shipment",field:"LastShipment", resizable: true },
    {headerName:"Next Demand",field:"NextDemand", resizable: true },
    {headerName:"Monthly Avarage",field:"MonthlyAvarage", resizable: true },
    {headerName:"OrderFreq. Forecast",field:"OrderFrequencybasedactual_forecast", resizable: true },
    {headerName:"Typeof Material Planing",field:"TypeofMaterialPlaning", resizable: true },
    {headerName:"Transfer Price",field:"TransferPrice", resizable: true },
    {headerName:"Replanishment Time",field:"ReplanishmentTime", resizable: true },
    {headerName:"ReOrder Point",field:"ReOrderPoint", resizable: true },
    {headerName:"Safety Stock",field:"SafetyStock", resizable: true },
    {headerName:"NET",field:"NET", resizable: true },
    {headerName:"New PO Quantity",field:"NewPOQuantity", resizable: true, cellStyle: { "font-weight":"bold" , color:'red'} },
    {headerName:"Min Pack Qty",field:"MinPackQty", resizable: true },
    {headerName:"PO Amount",field:"POAmount", filter: true, editable: true, resizable: true},
    {headerName:"W13",field:"W13", resizable: true },
    {headerName:"W17",field:"W17", resizable: true },
    {headerName:"W21",field:"W21", resizable: true }
];

  ngOnInit(): void {
    
  } 

//  <ArrayOfMRPDto>
//	<MRPDto>
//    <SupplierCode>TR00075555</SupplierCode>
//    <MRPDetails>
//    	<MRPDetailDto>
//    		<StockCode>20643-301</StockCode>	
//			<NewPOQuantity>55</NewPOQuantity>
//		</MRPDetailDto>
//    </MRPDetails>
//    </MRPDto>
//    <MRPDto>
//    <SupplierCode>TR00075555</SupplierCode>
//    <MRPDetails>
//    	<MRPDetailDto>
//    		<StockCode>71080-1</StockCode>	
//			<NewPOQuantity>2</NewPOQuantity>
//		</MRPDetailDto>
//    </MRPDetails>
//    </MRPDto>
//</ArrayOfMRPDto>

  getSelectedRows()
  {
    const selectedRows = this.agGrid.api.getSelectedNodes();
    const selectedData = selectedRows.map(node => node.data);
    

//     this.agGrid.api.getSelectedNodes().forEach(r=> {
//       this.items.push(
// " <ArrayOfMRPDto>" +
// "	<MRPDto>" +
// "   <SupplierCode>" + r.data.SupplierCode + "</SupplierCode>" +
// "   <MRPDetails>" +
// "   	<MRPDetailDto>" +
// "   		<StockCode>" + r.data.StockCode + "</StockCode>	" +
// "			<NewPOQuantity>" + r.data.NewPOQuantity + "</NewPOQuantity>" +
// "		</MRPDetailDto>" +
// "   </MRPDetails>" +
// "   </MRPDto>" +
// "/ArrayOfMRPDto>" );
//     });
    this.service.postPuchaseOrder().subscribe(res=> {
      alert(res.toString());
    });

  }

  onGridReady(params:any) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.refreshMRPList();

    this.gridApi.sizeColumnsToFit();
  }

  refreshMRPList(){
    this.service.getMRPDetails().subscribe(data =>{
      this.rowData = data;
    })
  }

  
  autoGroupColumnDef = {
    headerName: 'Supplier',
    field: 'Supplier',
    resizable: true,
    checkboxSelection: true,
    suppressSizeToFit: true,
    cellRenderer: 'agGroupCellRenderer',
    cellRendererParam: {
      checkbox: true
    }
  }

}
