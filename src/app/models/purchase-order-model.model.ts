

export class PurchaseOrderModel {
  public stockcode: string;
  public stockname: string;
  public monthlyavarage: number;
  public orderfrequencybasedactual_forecast: number;
  public typeofmaterialplaning: string;
  public supplier: string;
  public replanishmenttime: number;
  public reorderpoint: number;
  public safetystock: number;
  public NET: number;
  public newpoquantity: number;
  public minpackqty: number;
  public poamount: number;
  public W13: number;
  public W17: number;
  public W21: number;
}   
