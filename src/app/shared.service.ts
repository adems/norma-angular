import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { asapScheduler, asyncScheduler, Observable } from 'rxjs'; 
import { XmlParser } from '@angular/compiler';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  baseUrl = environment.apiUrl + 'MRP/'
  
  constructor(private http:HttpClient) {}

    getMRPDetails():Observable<any[]>{
    return this.http.get<any>(this.baseUrl+'GetMRP');
    }

    // postPuchaseOrder(purchaseOrderDetail:any[]){
    //   debugger;
    //   let asd =  this.http.post(this.APIUrl + '/MRP/AddMRPPurchaseOrder' , 
    //   purchaseOrderDetail[0]
    //   , {
    //     headers: new HttpHeaders({
    //       'Content-Type': 'application/xml'
    //     })
    //   });
    //   return asd;
    // }

    postPuchaseOrder():Observable<any[]>{
      return  this.http.get<any>(this.baseUrl + 'AddMRPPurchaseOrder');
       
    }


    getShipmentDetails():Observable<any[]>{
      return this.http.get<any>(this.baseUrl+'GetShipment');
    }

    postShipmentsForWaybill():Observable<any[]>{
        return this.http.get<any>(this.baseUrl+'AddWaybill');
    }
}
