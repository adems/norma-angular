import { Component, OnInit, ViewChild } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-shipment',
  templateUrl: './show-shipment.component.html',
  styleUrls: ['./show-shipment.component.css']
})
export class ShowShipmentComponent implements OnInit {

  @ViewChild("agGrid", {static: false }) agGrid: AgGridAngular;
  private gridApi:any=[];
  private gridColumnApi:any=[];
  
  constructor(private service:SharedService) { }

  rowData:any=[];

  columnDefs = [
    {headerName:"Sales Order No",field:"SalesOrderNo", sortable: true, rowGroup: true ,filter: true, resizable: true },
    {headerName:"Order Date",field:"OrderDate", sortable: true, filter: true, resizable: true},
    {headerName:"Delivery Date",field:"DeliveryDate", filter: true, resizable: true},
    {headerName:"Customer Code",field:"CustomerCode", filter: true,resizable: true },
    {headerName:"Customer Name",field:"CustomerName", filter: true,resizable: true },
    {headerName:"Shipping Address",field:"ShippingAddress", filter: true,resizable: true },
    {headerName:"Stock Code",field:"StockCode", filter: true,resizable: true },
    {headerName:"Stock Name",field:"StockName", filter: true,resizable: true },
    {headerName:"Customer Stock Code",field:"CustomerStockCode", filter: true,resizable: true },
    {headerName:"Waiting Amount",field:"WaitingAmount", filter: true,resizable: true },
    {headerName:"Deliverable Stock",field:"DeliverableStock", filter: true,resizable: true },
    {headerName:"Actual Stock",field:"ActualStock", filter: true,resizable: true },
    {headerName:"Goods On The Way",field:"GoodsOnTheWay", filter: true,resizable: true },
    {headerName:"Bonded Goods",field:"BondedGoods", filter: true,resizable: true },
    {headerName:"Trann Curr Amount",field:"TrannCurrAmount",filter: true, resizable: true},
    {headerName:"PO Amount",field:"POAmount",filter: true, resizable: true },
    {headerName:"Payment",field:"Payment", filter: true, editable: true, resizable: true},
    {headerName:"Delivery Method",field:"DeliveryMethod", resizable: true },
    {headerName:"Carrier",field:"Carrier",filter: true, resizable: true },
    {headerName:"Salesman",field:"Salesman",filter: true, resizable: true },
    {headerName:"TL Unit Price",field:"TLUnitPrice",filter: true, resizable: true },
    {headerName:"Current Rate",field:"CurrentRate",filter: true, resizable: true },
    {headerName:"Stock Control (Deliverable)",field:"StockControl_Deliverable",filter: true, resizable: true },
    {headerName:"Stock Control (Actual)",field:"StockControl_Actual",filter: true, resizable: true },
    {headerName:"PONo",field:"PONo", filter: true,resizable: true },
    {headerName:"POSupplyDate",field:"POSupplyDate",filter: true, resizable: true }
];


  ngOnInit(): void {
  }

  onGridReady(params:any) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.refreshShipmentManagementList();

    this.gridApi.sizeColumnsToFit();
  }

  refreshShipmentManagementList(){
    this.service.getShipmentDetails().subscribe(data =>{
      this.rowData = data;
    })
  }

  getSelectedRows()
  {
    this.service.postShipmentsForWaybill().subscribe(res=>{
      alert(res.toString());
    })
  }

  autoGroupColumnDef = {
    headerName: 'Sales OrderNo',
    field: 'SalesOrderNo',
    resizable: true,
    checkboxSelection: true,
    suppressSizeToFit: true,
    cellRenderer: 'agGroupCellRenderer',
    cellRendererParam: {
      checkbox: true
    }
  }


}
