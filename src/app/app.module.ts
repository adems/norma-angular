import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import "ag-grid-enterprise";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MRPManagementComponent } from './mrp-management/mrp-management.component';
import { ShowMRPComponent } from './mrp-management/show-mrp/show-mrp.component';
import { ShipmentManagementComponent } from './shipment-management/shipment-management.component';
import { ShowShipmentComponent } from './shipment-management/show-shipment/show-shipment.component';
import { SharedService } from './shared.service';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AgGridModule } from 'ag-grid-angular';


@NgModule({
  declarations: [
    AppComponent,
    MRPManagementComponent,
    ShowMRPComponent,
    ShipmentManagementComponent,
    ShowShipmentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AgGridModule.withComponents([])
  ],
  providers: [SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
