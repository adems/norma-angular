import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {MRPManagementComponent} from './mrp-management/mrp-management.component';
import {ShipmentManagementComponent} from './shipment-management/shipment-management.component';


const routes: Routes = [
  {path:'mrp-management', component:MRPManagementComponent},
  {path:'shipment-management', component:ShipmentManagementComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
